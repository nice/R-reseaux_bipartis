Analyse de réseaux bipartis
================
Marion Maisonobe
2019

``` r
library("igraph")
```

    ## 
    ## Attaching package: 'igraph'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     decompose, spectrum

    ## The following object is masked from 'package:base':
    ## 
    ##     union

``` r
library("tnet")
```

    ## Loading required package: survival

    ## tnet: Analysis of Weighted, Two-mode, and Longitudinal networks.
    ## Type ?tnet for help.

Ouvrir le fichier contenant la matrice

``` r
mat <- read.csv(file = "freeman_friendship.csv", header = T, sep = ";")
```

Remplacer les cases vides par des zéros

``` r
mat <- as.matrix(mat[,2:20])

mat[is.na(mat)] <- 0
```

Transformer la matrice en objet igraph

``` r
g <- graph_from_incidence_matrix(as.matrix(mat), directed = FALSE, multiple = FALSE,
                               weighted = NULL, add.names = NULL)
g
```

    ## IGRAPH d6a47f7 U--B 48 45 -- 
    ## + attr: type (v/l)
    ## + edges from d6a47f7:
    ##  [1]  2--41  2--42  2--43  3--40  3--42  3--44  3--45  4--32  4--33  5--46
    ## [11]  6--41  8--45  9--35  9--36 10--44 11--36 11--48 12--47 13--31 13--37
    ## [21] 13--38 13--48 14--34 15--30 15--37 16--30 17--43 18--46 19--37 19--38
    ## [31] 19--39 20--31 20--32 20--35 21--37 21--38 21--40 23--47 24--39 25--33
    ## [41] 25--34 26--38 28--41 28--42 29--45

L’attribut “type” permet de différencier les deux ensembles de sommets
Numéroter les membres du réseau

``` r
V(g)[V(g)$type == 0]$name <- 1:29
```

Numéroter les évènements

``` r
V(g)[V(g)$type == 1]$name <- paste("E",1:19, sep = "" )

vertex_attr(g, "label") <- V(g)$name
```

Symboliser les évènements par des carrés et les individus par des
cercles

``` r
V(g)[V(g)$type == 1]$shape <- "square"
V(g)[V(g)$type == 0]$shape <- "circle"
```

Choisir deux gammes de couleur pour bien distinguer les deux ensembles
de sommets

``` r
col <- c("steelblue", "orange")
```

Suppression des isolés

``` r
g1 <- delete.vertices(g, V(g)[degree(g) == 0])

plot(g1,
     vertex.color = col[as.numeric(V(g1)$type)+1],
     vertex.label.cex = 0.6,
     main = "Réseaux acteurs-évènements",
     sub = "Les isolés ne sont pas représentés"
     ,layout = layout.fruchterman.reingold(g1, niter = 10000) #layout_as_bipartite
)
```

![](script_bipartis_knit_files/figure-gfm/unnamed-chunk-9-1.png)<!-- -->

Mesurer le degré de l’ensemble des sommets

``` r
V(g)$degre <- degree(g)
```

Degré des évènements : combien d’individus ont participé à chaque
évènement ?

``` r
V(g)[V(g)$type == 1]$degre
```

    ##  [1] 2 2 2 2 2 2 2 4 4 2 2 3 3 2 2 3 2 2 2

Degré des individus : à combien d’évènements ont participé chaque
individu ?

``` r
V(g)[V(g)$type == 0]$degre
```

    ##  [1] 0 3 4 2 1 1 0 1 2 1 2 1 4 1 2 1 1 1 3 3 3 0 1 1 2 1 0 2 1

Extraire la liste des composantes connexes

``` r
comp <- components(g)
```

Repérer l’identifiant de la composante principale

``` r
cp <- which(comp$csize %in% max(comp$csize))
```

Associer aux sommets du graphe, le vecteur d’appartenance à une
composante

``` r
V(g)$membership <- comp$membership
```

Compter le nombre d’individus dans la composante principale

``` r
vcount(induced.subgraph(g,V(g)[V(g)$type == 0 & V(g)$membership == cp]))
```

    ## [1] 21

Compter le nombre d’évènements dans la composante principale

``` r
vcount(induced.subgraph(g,V(g)[V(g)$type == 1 & V(g)$membership == cp]))
```

    ## [1] 17

Transformer le graphe en liste de liens pour appliquer les fonctions du
package tnet

``` r
net <- get.edgelist(g,names = F)
```

Calculer la matrice des plus courts chemins entre individus

``` r
mat_pcc <- distance_tm(as.tnet(net), projection.method = "sum")
```

    ## Warning in as.tnet(net): Data assumed to be binary two-mode tnet (if this is not
    ## correct, specify type)

Trouver la longueur du plus long des plus courts chemins

``` r
max(mat_pcc[,1:21], na.rm = T)
```

    ## [1] 8.467742

*Différentes méthodes de projection*  
Obtenir le réseau des individus non-valué

``` r
onemode1 <- projecting_tm(net, method = "binary")
```

Obtenir le réseau des individus valué en fonction du nombre d’évènements
partagés

``` r
onemode2 <- projecting_tm(net, method = "sum")
```

Obtenir le réseau des individus valué en fonction du nombre d’évènements
partagés et pondéré en fonction du nombre de participants à chaque
évènement; l’hypothèse sous-jacente étant que moins il y a de monde
dans un évènement, plus il y a de chance que les participants se soient
parlés

``` r
onemode3 <- projecting_tm(net, method = "Newman")
```

Différents indices de centralité Calcul du degré des individus

``` r
degree_tm <- degree_tm(net, measure = "degree")
```

Calcul du coefficient de reinforcement

``` r
reinforcement_tm(net)
```

    ## [1] 0.1568627

Calcul du clustering coefficient

``` r
clustering_tm(net)
```

    ## [1] 0

``` r
net2 <- net[,2:1]
```

-----

``` r
library("bipartite")
```

    ## Loading required package: vegan

    ## Loading required package: permute

    ## 
    ## Attaching package: 'permute'

    ## The following object is masked from 'package:igraph':
    ## 
    ##     permute

    ## Loading required package: lattice

    ## This is vegan 2.5-6

    ## 
    ## Attaching package: 'vegan'

    ## The following object is masked from 'package:igraph':
    ## 
    ##     diversity

    ## Loading required package: sna

    ## Loading required package: statnet.common

    ## 
    ## Attaching package: 'statnet.common'

    ## The following object is masked from 'package:base':
    ## 
    ##     order

    ## Loading required package: network

    ## network: Classes for Relational Data
    ## Version 1.16.1 created on 2020-10-06.
    ## copyright (c) 2005, Carter T. Butts, University of California-Irvine
    ##                     Mark S. Handcock, University of California -- Los Angeles
    ##                     David R. Hunter, Penn State University
    ##                     Martina Morris, University of Washington
    ##                     Skye Bender-deMoll, University of Washington
    ##  For citation information, type citation("network").
    ##  Type help("network-package") to get started.

    ## 
    ## Attaching package: 'network'

    ## The following objects are masked from 'package:igraph':
    ## 
    ##     %c%, %s%, add.edges, add.vertices, delete.edges, delete.vertices,
    ##     get.edge.attribute, get.edges, get.vertex.attribute, is.bipartite,
    ##     is.directed, list.edge.attributes, list.vertex.attributes,
    ##     set.edge.attribute, set.vertex.attribute

    ## sna: Tools for Social Network Analysis
    ## Version 2.6 created on 2020-10-5.
    ## copyright (c) 2005, Carter T. Butts, University of California-Irvine
    ##  For citation information, type citation("sna").
    ##  Type help(package="sna") to get started.

    ## 
    ## Attaching package: 'sna'

    ## The following objects are masked from 'package:igraph':
    ## 
    ##     betweenness, bonpow, closeness, components, degree, dyad.census,
    ##     evcent, hierarchy, is.connected, neighborhood, triad.census

    ##  This is bipartite 2.15.
    ##  For latest changes see versionlog in ?"bipartite-package". For citation see: citation("bipartite").
    ##  Have a nice time plotting and analysing two-mode networks.

    ## 
    ## Attaching package: 'bipartite'

    ## The following object is masked from 'package:vegan':
    ## 
    ##     nullmodel

    ## The following objects are masked from 'package:tnet':
    ## 
    ##     as.tnet, betweenness_w, closeness_w, clustering_tm, projecting_tm,
    ##     symmetrise_w, tnet_igraph

    ## The following object is masked from 'package:igraph':
    ## 
    ##     strength

Ouvrir le fichier contenant la matrice

``` r
mat <- read.csv(file = "freeman_friendship.csv", header = T, sep = ";")
```

Transformer en objet de type matrice

``` r
mat <- as.matrix(mat[1:29,2:20])
```

Renommer les colonnes (évènements)

``` r
colnames(mat) <- paste("E",1:19, sep = "" )
```

Remplacer les cases vides par des zéros

``` r
mat[is.na(mat)] = 0
```

Représenter l’information

``` r
plotweb(mat)
```

![](script_bipartis_knit_files/figure-gfm/unnamed-chunk-32-1.png)<!-- -->

Mesures

``` r
networklevel(mat)
```

    ##                       connectance                     web asymmetry 
    ##                      9.473684e-02                     -1.363636e-01 
    ##                 links per species            number of compartments 
    ##                      1.022727e+00                      3.000000e+00 
    ##             compartment diversity               cluster coefficient 
    ##                      1.636938e+00                      8.000000e-02 
    ##                        nestedness                              NODF 
    ##                      2.943043e+01                      5.590941e+00 
    ##               weighted nestedness                     weighted NODF 
    ##                     -5.707073e-02                      0.000000e+00 
    ##    interaction strength asymmetry          specialisation asymmetry 
    ##                      0.000000e+00                      1.684896e-01 
    ##                   linkage density              weighted connectance 
    ##                      2.444444e+00                      5.555556e-02 
    ##                      Fisher alpha                 Shannon diversity 
    ##                      8.589935e+09                      3.806662e+00 
    ##              interaction evenness      Alatalo interaction evenness 
    ##                      6.176323e-01                      1.000000e+00 
    ##                                H2              number.of.species.HL 
    ##                      0.000000e+00                      1.900000e+01 
    ##              number.of.species.LL mean.number.of.shared.partners.HL 
    ##                      2.500000e+01                      1.754386e-01 
    ## mean.number.of.shared.partners.LL            cluster.coefficient.HL 
    ##                      1.166667e-01                      1.022222e-01 
    ##            cluster.coefficient.LL   weighted.cluster.coefficient.HL 
    ##                      1.228070e-01                      0.000000e+00 
    ##   weighted.cluster.coefficient.LL                  niche.overlap.HL 
    ##                      0.000000e+00                      6.812865e-02 
    ##                  niche.overlap.LL                   togetherness.HL 
    ##                      5.388889e-02                      4.358118e-02 
    ##                   togetherness.LL                        C.score.HL 
    ##                      2.977778e-02                      8.903103e-01 
    ##                        C.score.LL                        V.ratio.HL 
    ##                      9.130093e-01                      2.133333e-01 
    ##                        V.ratio.LL                    discrepancy.HL 
    ##                      5.940594e-01                      3.600000e+01 
    ##                    discrepancy.LL               extinction.slope.HL 
    ##                      3.300000e+01                      1.580470e+00 
    ##               extinction.slope.LL                     robustness.HL 
    ##                      2.303682e+00                      6.112844e-01 
    ##                     robustness.LL     functional.complementarity.HL 
    ##                      6.978915e-01                      3.402716e+01 
    ##     functional.complementarity.LL              partner.diversity.HL 
    ##                      3.431452e+01                      8.974664e-01 
    ##              partner.diversity.LL                     generality.HL 
    ##                      7.242549e-01                      2.555556e+00 
    ##                  vulnerability.LL 
    ##                      2.333333e+00

Mesures au niveau des liens

``` r
indices_link <- linklevel(mat)
```

Calculer le degré normalisé

``` r
ND(mat, normalised = T )
```

    ## $lower
    ##          1          2          3          4          5          6          7 
    ## 0.00000000 0.15789474 0.21052632 0.10526316 0.05263158 0.05263158 0.00000000 
    ##          8          9         10         11         12         13         14 
    ## 0.05263158 0.10526316 0.05263158 0.10526316 0.05263158 0.21052632 0.05263158 
    ##         15         16         17         18         19         20         21 
    ## 0.10526316 0.05263158 0.05263158 0.05263158 0.15789474 0.15789474 0.15789474 
    ##         22         23         24         25         26         27         28 
    ## 0.00000000 0.05263158 0.05263158 0.10526316 0.05263158 0.00000000 0.10526316 
    ##         29 
    ## 0.05263158 
    ## 
    ## $higher
    ##         E1         E2         E3         E4         E5         E6         E7 
    ## 0.06896552 0.06896552 0.06896552 0.06896552 0.06896552 0.06896552 0.06896552 
    ##         E8         E9        E10        E11        E12        E13        E14 
    ## 0.13793103 0.13793103 0.06896552 0.06896552 0.10344828 0.10344828 0.06896552 
    ##        E15        E16        E17        E18        E19 
    ## 0.06896552 0.10344828 0.06896552 0.06896552 0.06896552

Modèle nul

``` r
modele_nul <- mgen(mat)
```

    ## This is not a probability matrix! I will proceed after transforming the entries according to option 'autotransform': sum !

``` r
modele_nul <- nullmodel(N = 5, mat, method = "mgen")
```

    ## This is not a probability matrix! I will proceed after transforming the entries according to option 'autotransform': equiprobable !

    ## This is not a probability matrix! I will proceed after transforming the entries according to option 'autotransform': equiprobable !
    ## This is not a probability matrix! I will proceed after transforming the entries according to option 'autotransform': equiprobable !
    ## This is not a probability matrix! I will proceed after transforming the entries according to option 'autotransform': equiprobable !
    ## This is not a probability matrix! I will proceed after transforming the entries according to option 'autotransform': equiprobable !
