# Contenu

- __[Introduction](#introduction)__
- __[Définition et histoire du graphe bipartite](#d%C3%A9finition-et-histoire-du-graphe-bipartite)__
- __[L'exemple du réseau analysé par Linton Freeman](#lexemple-du-r%C3%A9seau-analys%C3%A9-par-linton-freeman)__
- __[Applications sur le logiciel R](#applications-sur-le-logiciel-r)__
  - [Le package _igraph_](#charger-les-packages)  
      - [Charger les données](#charger-les-données)  
      - [Changer les noms des sommets en fonction de leur type](#changer-les-noms-des-sommets-en-fonction-de-leur-type)  
      - [Représenter le réseau bipartite](#représenter-le-réseau-bipartite)  
      - [Indicateurs](#indicateurs)  
  - [Le package _tnet_](#le-package-tnet)  
      - [Projection vers un réseau one-mode](#projection-vers-un-réseau-one-mode)  
      - [Indices de centralité](#indices-de-centralité)  
  - [Le Package _bipartite_](#le-package-bipartite)  
      - [Charger le package et les données](#charger-le-package-et-les-données)  
      - [Indicateurs](#indicateurs-1)  
      - [Modélisation](#modélisation)  
      - [Partionnement](#partionnement)  
      - [Pour aller plus loin](#pour-aller-plus-loin)
- __[Exercice](#exercice)__
- __[Réferences](#références)__

# Introduction

Les articles sur les réseaux bipartites insistent sur la richesse, pour la compréhension du social, de ce type d’objets mathématiques et regrettent le manque d’algorithmes et métriques permettant de les analyser.

Voici trois phrases extraites de l'introduction de trois articles différents parus au cours des 10 dernières années :

>« _Many large real-world networks actually have a two-mode nature_ »

>« _Some real-world datasets have natural bipartite structure_ »

>« _As a new frontier of complex network, multi-mode network has been shown to better represent reality according to its heterogeneous attributes_ »

**Deux éléments au moins posent problème dans ces affirmations :**

1.  la volonté d’analyser des réseaux « _multi-mode_ », de tenir compte des effets de multi-appartenance et de multiplexité n’est pas une volonté nouvelle en analyse de réseaux sociaux.
2.  le réseau qu’il soit « _one-mode_ », « _two-mode_ », « _three-mode_ » etc. n’est pas quelque chose que l’on trouve tel quel dans la réalité. Les « faits sociaux » n’ont pas « par nature » telle ou telle « structure mathématique » qu’il faudrait que nous expliquions.

À l’appui de cela, considérons un extrait de l’article de Narciso Pizarro «_Appartenances, places et réseaux de places : La reproduction des processus sociaux et la génération d’un espace homogène pour la définition des structures sociales_», paru en 2000 : 

>« Nous voulons souligner une caractéristique, à nos yeux, essentielle, des rapports sociaux en tant que _faits à expliquer_ : c’est qu’ils ne sont en aucun cas des _faits bruts_, des _données_ bêtement livrées en pâture aux chercheurs par une quelconque nature du social. Et donc, que toute construction ultérieure, à partir de ces _données_, est une _construction sur une construction_.»

Afin de rediscuter ces affirmations et mieux comprendre les enjeux associés à l’analyse des réseaux bipartites, faisons un petit détour historique dans le champ de l’analyse des réseaux sociaux. Ce détour permettra d’expliquer pourquoi l’analyse de réseaux bipartites est longtemps restée associée, en analyse des réseaux sociaux, à l’analyse des « réseaux d’appartenance », en anglais « _membership networks_ » ou « _affiliation networks_ ».

Ensuite, nous verrons quelques indicateurs et méthodes développées pour analyser des graphes bipartites avec le package R _igraph_. Nous verrons comment projeter des graphes bipartites en graphe unimode avec le package R _tnet_ et les choix méthodologiques associés à cette étape de projection. 
Nous finirons en explorant les possibilités du package R _bipartite_ en matière de visualisation, d'indicateurs et de partitionnement. Tout au long de cette intervention, un exemple sera traité, celui d’un réseau de liens professionnels (avoir été collègues dans une même institution) entre spécialistes de l’analyse de réseaux sociaux. Il est tiré d'un travail de Linton Freeman (1980) réalisé à partir d'une enquête assez célèbre sur l'amitié entre spécialistes de l'analyse de réseaux sociaux à la fin des années 1970.

# Définition et histoire du graphe bipartite

Qu’appelle-t-on graphe bipartite ? Les graphes bipartites permettent de formaliser les relations existantes entre deux populations distinctes (deux ensembles distincts de sommets). On trouve parfois pour les désigner les termes de réseaux _two-mode_,  _hypernetworks_, et _dual networks_.

Selon la nature des ensembles de sommets considérés, les sociologues anglophones distinguent les _affiliations network_ (ex. appartenance d’individus à des associations ou des organisations) et les _actors-events network_ (ex. participation d’individus à des événements).

L'exemple classique dans la littérature sociologique porte sur la présence de femmes à des évènements sociaux (Davis _et al._, 1941). Ronald Breiger s'appuie sur les données de cette étude dans un article paru en 1974 pour proposer une approche qu'il nomme _membership network analysis_. À partir de là, l'étude des réseaux bipartis reste associée, en sociologie, à la distinction conceptuelle entre relation d'appartenance et relation sociale.

Il s'agit d'interroger l'influence qu'exercent les appartenances et les évènements sur l'activation et la stabilité des relations sociales. Tout un ensemble de travaux se développe autour de ces questions. Ils traitent notamment de la co-présence des dirigeants à des conseils d'administration. L'objectif est d'en déduire la structure des relations sociales entre dirigeants. On parle pour qualifier cette catégorie de réseaux de _corporate interlocks_. 

Intéressé par ces questions, le sociologue Narciso Pizarro insiste sur la nécessité d'analyser non seulement les relations entre individus obtenues à partir du réseau bipartite mais aussi le réseau des relations entre "places" occupées par ces individus, places que l'on peut déduire de leurs positions dans le réseau bipartite. Narciso Pizarro ne considère pas qu'il y ait une différence de nature entre relation d'appartenance et relation inter-individuelle. Pour lui, la relation inter-individuelle est déterminée par la position occupée par les individus dans un réseau de "places".

Récemment, avec le développement de l'analyse de réseaux appliquée aux grandes bases de données disponibles en ligne, de nouvelles méthodes d'analyse et de nouveaux types de réseaux bipartites sont analysés. Par exemple, à partir du site internet _imdb_, il est aisé de générer et d'analyser le graphe de co-présence des acteurs dans des films. Les graphes de co-occurences de mots dans des livres et de participation à des forums en ligne font également partie des exemples donnés pour l'analyse de grands graphes bipartites par Mathieu Latapy _et al._ (2008).

Traditionnellement, les écologues sont également intéressés par l'étude des graphes bipartites. Ils s'intéressent aux relations entre plantes et polinisateurs ainsi qu'aux relations de prédation entre espèces différentes. Des packages R a été développé spécialement pour analyser ce type de réseau, le package _bipartite_ et le package _bmotif_. Le premier s'appuie en partie sur les options développées par Tore Opsahl dans le package _tnet_, mais il propose aussi une série de mesures adaptées aux recherches en écologie. Un moyen de visualisation est aussi proposé que nous testerons dans la partie pratique du cour. Le second permet de détecter et d'analyser les motifs présents dans le graphe (sous-graphes).

En bibliométrie, l'analyse de réseaux a toujours occupé une place importante. Les corpus documentaires peuvent être analysés sous différents angles : liens entre mots, auteurs, affiliations, revues, disciplines etc. Souvent, l'information bibliographique est structurée de telle sorte que l'on part de la relation entre les articles d'un côté et de l'autre, les mots ou auteurs ou revues qui se rattachent à ces articles.

## Caractéristiques du graphe bipartite

Mathématiquement, un graphe bipartite se définit comme un triplet G=(V1, V2, E), avec V1 et V2 des ensembles disjoints de sommets, 
et E ⊆ V1 × V2, l'ensemble des liens entre V1 et V2.

Si le graphe bipartite décrit les relations entre deux ensembles distincts, il ne prend pas en compte les relations à l’intérieur de ces deux ensembles. Par ailleurs, si les relations au sein d’un graphe bipartite sont généralement non orientées, elles peuvent par contre être valuées.

# L'exemple du réseau analysé par Linton Freeman

L'originalité du graphe bipartite tiré de l'article de Linton Freeman est d'avoir été généré à partir d'un réseau _one-mode_ selon une méthode quelque peu oubliée qu'il est possible de retrouver dans [l'article](https://framagit.org/MarionMai/R-reseaux_bipartis/blob/master/freeman_friendship_80.pdf). 

# Applications sur le logiciel R

```{r, include=FALSE}
setwd("F:/Lexar/ECOLE_THEM_GDR/BIPARTIS")
getwd()
```

# Charger les packages

```{r, warning=F, message=F}
library("igraph")
library("tnet")
```

Nous chargerons le package _bipartite_ plus tard pour éviter un bug lié à l'incompatibilité entre le package _sna_, sur lequel s'appuie le package _bipartite_, et le package _igraph_.

## Charger les données

```{r}

#ouvrir le fichier contenant la matrice
mat<-read.csv(file="freeman_friendship.csv", header=T, sep=";")

#remplacer les cases vides par des zéros
mat<-as.matrix(mat[,2:20])

mat[is.na(mat)] = 0

#transformer la matrice en objet igraph
g<-graph_from_incidence_matrix(as.matrix(mat), directed = FALSE, multiple = FALSE, 
                            weighted = NULL, add.names = NULL)
g

```

## Paramètres

Les paramètres du graphe créé s'affichent. Ce graphe est "U--B", U pour "Undirected" et B pour"Bipartite" ce qui veut dire que ce graphe est non dirigé et bipartite.

48 correspond au nombre de sommets et 45 au nombre de liens.

## Changer les noms des sommets en fonction de leur type

```{r}

#l'attribut "type" permet de différencier les deux ensembles de sommets

#numéroter les membres du réseau

V(g)[V(g)$type == 0]$name<-1:29

#numéroter les évènements

V(g)[V(g)$type == 1]$name<-paste("E",1:19, sep="" )

vertex_attr(g, "label") <- V(g)$name

```

## Représenter le réseau bipartite

```{r}

#symboliser les évènements par des carrés et les individus par des cercles

V(g)[V(g)$type == 1]$shape <- "square"
V(g)[V(g)$type == 0]$shape <- "circle"

#choisir deux gammes de couleur pour bien distinguer les deux ensembles de sommets

col <- c("steelblue", "orange")

#suppression des isolés
g1 <- delete.vertices(g, V(g)[degree(g)==0])

plot(g1,
  vertex.color = col[as.numeric(V(g1)$type)+1],
  vertex.label.cex=0.6,
  main= "Réseaux acteurs-évènements",
  sub= "Les isolés ne sont pas représentés"
  ,layout=layout.fruchterman.reingold(g1, niter=10000) #layout_as_bipartite
)

```


## Indicateurs

```{r}

#mesurer le degré de l'ensemble des sommets
V(g)$degre <- degree(g)

#degré des évènements: combien d'individus ont participé à chaque évènement ?
V(g)[V(g)$type == 1]$degre

```

```{r}
#degré des individus: à combien d'évènements ont participé chaque individu ?
V(g)[V(g)$type == 0]$degre
```
```{r}

#extraire la liste des composantes connexes
cp <- components(g)

#repérer l'identifiant de la composante principale

X<-which(cp$csize==max(cp$csize))

#associer aux sommets du graphe, le vecteur d'appartenance à une composante 

V(g)$membership<-cp$membership

#compter le nombre d'individus dans la composante principale
vcount(induced.subgraph(g,V(g)[V(g)$type == 0 & V(g)$membership==X]))

#compter le nombre d'évènements dans la composante principale
vcount(induced.subgraph(g,V(g)[V(g)$type == 1 & V(g)$membership==X]))


```

# Le Package _tnet_  

Dans le cadre de sa thèse, Tore Opsahl a mis au point de nombreux indicateurs pour l'analyse des réseaux bipartites, valués, et longitudinaux.
Les fonctions permettant de calculer ces indicateurs sont implémentées dans le package _tnet_ qu'il a développé.
Pour utiliser ce package, il faut transformer le graphe à analyser en une liste de liens.   


```{r results="hide", warning=F, error=F, message=F, comment=F}

#transformer le graphe en liste de liens pour appliquer les fonctions du package tnet

net<-get.edgelist(g,names=F)

# calculer la matrice des plus courts chemins entre individus

mat_pcc<-distance_tm(as.tnet(net), projection.method="sum")

```
  
La matrice obtenue donne la valeur des plus courts chemins entre les 21 individus compris dans la composante principale du graphe bipartite.
De plus amples informations sont disponibles [sur son site](https://toreopsahl.com/tnet/two-mode-networks/shortest-paths/).
  
```{r }

#trouver la longueur du plus long des plus courts chemins

max(mat_pcc[,1:21], na.rm=T)

```

## Projection vers un réseau _one-mode_

Dans la majorité des recherches portant sur les réseaux bipartites, tant en sciences sociales qu'en écologie, l'option privilégiée consiste à transformer le réseau bipartite en réseau _one-mode_, plus aisé à analyser.

Dans le cas d'un réseau "individus-évènement" comme celui que l'on manipule ici, cela revient à s'intéresser uniquement aux relations entre individus, ou uniquement aux relations entre évènements. Ces relations sont déduites du réseau bipartite et il existe plusieurs moyens de les pondérer.

Dans sa thèse Tore Opsahl a réfléchi aux effets des choix de projection. Il en a retenu trois qu'il est possible d'appliquer à l'aide du package _tnet_.
De plus amples informations sont disponibles [sur son site](https://toreopsahl.com/tnet/two-mode-networks/projection/).

Il distingue trois méthodes :

- **La méthode "binary"**

Elle permet de passer d'un réseau bipartite à un réseau _one-mode_ non valué. Dans le cas d'un réseau "individus-évènements", cela revient à relier les individus ayant participé aux mêmes évènements sans tenir compte du nombre d'évènements partagés.

- **La méthode "sum"**

Elle permet de passer d'un réseau bipartite à un réseau _one-mode_ valué. Dans le cas d'un réseau "individus-évènements" non pondéré, cela revient à relier les individus ayant participé aux mêmes évènements en tenant compte du nombre d'évènements partagés. Dans le réseau que nous prenons ici pour exemple, pratiquement tous les individus n'ont qu'un seul évènement en commun. Seuls les évènements E8 et E9 ont été partagés par les mêmes individus (13, 19 et 21). Dans ce cas, les liens unissant ces trois individus prendront pour valeur 2, soit le nombre d'évènements partagés.

Le réseau bipartite de départ peut-être valué, par exemple si l'on considère un réseau entre acheteurs et produits puisqu'il est possible que les acheteurs achètent plusieurs fois le même produit. Dans ce cas là, le réseau _one-mode_ obtenu est un réseau à liens multiples. Si deux individus A et B ont acheté le même produit mais que A l'a acheté en 4 exemplaires et B en 6 exemplaires alors, dans le réseau multiplexe obtenu, le lien dirigé allant de A vers B vaudra 4 et le lien dirigé allant de B vers A vaudra 6. 

- **La méthode "Newman"**

Cette méthode permet de pondérer les relations en fonction du nombre de participants aux évènements. L'idée sousjacente est que la valeur du lien dyadique entre deux individus est d'autant plus forte qu'ils sont peu nombreux à avoir participé au même évènement. Si tous les individus du réseau ont participé au même évènement, l'importance des relations dyadiques que l'on peut déduire de la participation à cet évènement est plus faible. 

Ce type de pondération a été proposé par M. Newman (2001) pour étudier des réseaux de collaboration entre chercheurs à partir d'un réseau bipatite de co-signature d'articles (auteurs-articles). L'idée est que l'importance du lien de collaboration entre deux auteurs varie en fonction du nombre d'auteurs ayant participé à l'écriture des articles qu'ils ont co-signés.

Ce type de pondération est volontiers utilisé par les écologues pour étudier des réseaux de type plantes-polinisateurs. Appliqué à ces réseaux, cela revient à dire que deux plantes ayant été visitées par un nombre très important d'espèces de polinisateurs partagent une moins forte relation que deux plantes ayant été visitées par un petit nombre d'espèces de polinisateurs (Padron _et al._, 2011).

La pondération proposée par M. Newman a été pensée afin que le degré pondéré de chaque sommet dans le réseau _one-mode_ soit égal au nombre de sommets auquel il était relié dans le réseau _two-mode_. 

Il existe une variante de cette pondération que j'utilise dans mon travail sur les réseaux de collaboration scientifique entre villes (Maisonobe _et al._, 2016). Dans cette variante, ce n'est pas le degré pondéré de chaque sommet qui permet de retrouver le nombre d'articles co-signés, mais c'est la somme de la valeur de l'ensemble des liens du réseau qui permet de retrouver le nombre total d'articles considérés pour obtenir le réseau. Cela permet d'éviter les doubles comptes lorsque l'on somme l'ensemble des relations et qu'on analyse le réseau dans sa globalité.

Plutôt que de fractionner les liens par le nombre de sommets impliqués dans l'évènement moins un (1 / (Np - 1)), cela revient à fractionner les liens par le nombre exact de sommets impliqués dans l'évènement (1 / Np).  

```{r}

#différentes méthodes de projection

#obtenir le réseau des individus non-valué

onemode1<-projecting_tm(net, method="binary")

#obtenir le réseau des individus valué en fonction du nombre d'évènements partagés

onemode2<-projecting_tm(net, method="sum")

#obtenir le réseau des individus valué en fonction du nombre d'évènements partagés 
#et pondéré en fonction du nombre de participants à chaque évènement
#l'hypothèse sous-jacente étant que moins il y a de monde dans un évènement, 
#plus il y a de chance que les participants se soient parlés

onemode3<-projecting_tm(net, method="Newman")

```

## Indices de centralité

- **La centralité de degré**

Tore Opshal propose une fonction degré appelée _degree_tm_ qui est adaptée aux réseaux _2-mode_. Elle donne un vecteur spécifiant le degré de l'un des deux ensembles de sommets du graphe. Appliquée à notre exemple, cette mesure indique pour chaque individu le nombre d'évènements auxquels il s'est connecté. Nous l'avons déjà calculé avec le package _igraph_.

- **Le _clustering coefficient_**

Calculer le _clustering coefficient_ n'a pas de sens dans un réseau _2-mode_ puisque par définition, il ne peut pas y avoir de triangles dans un réseau _2-mode_. Toutefois, plusieurs variantes ont été imaginées dont celle de Robins et Alexander (2004). Ces derniers ont défini un coefficient de _clustering_ qui fait le ratio entre le nombre de 4-cycles (le plus petit cycle possible dans un réseau _2-mode_) et le nombre de chemin de longueur 3. Dans le cas d'un réseau "acteur-évènement", cela revient à mesurer la tendance pour les couples d'individus ayant participé à un évènement en commun, d'avoir participé à un second évènement en commun. Cette mesure est appelé "_reinforcement_" dans le package _tnet_.

Considérant que cette proposition s'éloignait trop du principe initial du _clustering coefficient_, Tore Opshal a proposé une mesure de _clustering coefficient_ pour les réseaux _2-mode_ qu'il jugeait plus en adéquation avec celle du _clustering coefficient_ pour les réseaux _one-mode_ (nombre de triangles fermés/nombre de triangles possibles). Ce coefficient fait le ratio entre le nombre de chemin de longueur 4 fermés par des 6-cycles sur le nombre de chemins de longueur 4 possibles. Une version de ce coefficient a été imaginée pour les réseaux _2-mode_ valués. Cette mesure permet de mesurer la connectivité du graphe par rapport à une connectivité possible.

```{r}

#différents indices de centralité

#calcul du degré des individus

degree_tm <- degree_tm(net, measure="degree")

#calcul du coefficient de reinforcement

reinforcement_tm(net)

#calcul du clustering coefficient

clustering_tm(net)


```

Le coefficient de _reinforcement_ donne une mesure globale de connectivité du graphe bipartite. Le _clustering coefficient_ adapté aux réseaux _2-mode_ est nul pour l'exemple présent car il n'y a pas de 6-cycle dans le réseau que nous analysons.


- **Centralités de proximité et intermédiarité**

Les indicateurs de centralité de proximité (_closeness_) et d'intermédiarité (_betweenness_) proposés par Tore Opshal pour l'analyse des réseaux _2-mode_ sont en fait calculés à partir de projections du réseau _2-mode_ en réseau _one-mode_. Puisque cela nous ramène à un problème d'analyse de réseau _one-mode_, nous ne les traitons pas ici.

- **Astuce**

On aura remarqué que les indices de centralité locaux s'appliquent par défaut à un seul des deux ensembles du graphe bipartite, en l'occurrence celui qui correspond aux individus. Si l'on souhaite plutôt travailler sur le second ensemble, en l'occurence celui qui correspond aux évènements, il suffira d'inverser les colonnes de la liste de liens.

```{r}
net2 <- net[,2:1]

```

# Le Package _bipartite_

## Charger le package et les données

```{r, warning=F, message=F}
library("bipartite")

#ouvrir le fichier contenant la matrice
mat<-read.csv(file="freeman_friendship.csv", header=T, sep=";")

#transformer en objet de type matrice
mat<-as.matrix(mat[1:29,2:20])

#renommer les colonnes (évènements)
colnames(mat)<- paste("E",1:19, sep="" )

#remplacer les cases vides par des zéros
mat[is.na(mat)] = 0

#représenter l'information
plotweb(mat)

```

## Indicateurs

Le package _bipartite_ dispose d'une fonction permettant d'obtenir toute une batterie d'indicateurs adaptés à l'étude des réseaux bipartites en écologie.  

```{r}

#mesures
networklevel(mat)

```

Les 18 premiers indicateurs sont des indicateurs globaux (qui concernent l'ensemble du réseau). Les indicateurs suivants portent sur chacune des deux populations du graphes. "HL"signifie "_Higher level_" et désigne la population en colonne, en l'occurence les évènements. "LL" signifie "_Lower level_" et désigne la population en ligne, en l'occurence les individus. On trouve la définition de chaque indicateur dans la documentation du package.

Une fonction du même type permet de mesurer des indicateurs portant non pas sur les sommets mais sur les liens du graphes.

```{r}

#mesures
indices_link<-linklevel(mat)

```

Les résultats sont stockés dans des matrices.


La fonction _ND_ permet de calculer le degré normalisé par le nombre d'éléments de chaque population.

```{r}

#calculer le degré normalisé
ND(mat, normalised = T )

```

## Modélisation

La fonction _mgen_ permet de générer un modèle nul à savoir une matrice de même taille dans laquelle les liens ont la même probabilité d'apparition que dans la matrice analysée. On peut aussi mobiliser cette méthode en utilisant la fonction _nullmodel_.

```{r, warning=F, message=F}

#modèle nul
modele_nul<-mgen(mat)

modele_nul<-nullmodel(N=5, mat, method="mgen")

```


## Partionnement

La fonction _computeModules_ permet de repérer des sous-groupes très connectés dans un réseau _two-mode_ valué en utilisant la valeur de la modularité proposée par Newman. La fonction _plotModuleWeb_ permet de visualiser le résultat de ce partitionnement.

```{r}

#trouver des partitions
mod_mat<-computeModules(mat)

plotModuleWeb(mod_mat)
```
  
# Pour aller plus loin

D'autres fonctionnalités du package _bipartite_ ont été testées par Camille Brisson et Chloé Migayrou dans le cadre d’un cours d’analyse de réseaux dispensé par Laurent Beauguitte à l’ENSAI. Leur travail a donné lieu à un billet dans le carnet de recherche du GDR AR-SHS accessible au lien suivant: [http://arshs.hypotheses.org/260](http://arshs.hypotheses.org/260)

# Exercice  

Comparer la filmographie des [frères Cohen](https://fr.wikipedia.org/wiki/Joel_et_Ethan_Coen) et celle de [Scorsese](https://fr.wikipedia.org/wiki/Martin_Scorsese). Les pages Wikipédia de ces deux réalisateurs nous indiquent la liste des acteurs revenant régulièrement dans leurs films. Analyser la structure des réseaux de co-apparition des acteurs dans les films de ces deux réalisateurs en utilisant les packages _igraph_, _tnet_ et _bipartite_.

# Références

Breiger, R. L. (1974). The Duality of Persons and Groups. Social Forces, 53(2), 181‑190. https://doi.org/https://doi.org/10.2307/2576011

Davis, A., Gardner, B. B., & Gardner, M. R. (1941). Deep South: A Social Anthropological Study of Caste and Class. University of South Carolina Press. Consulté à l’adresse https://books.google.fr/books?id=Q3b9QTOgLFcC

Freeman, L. C. (1980). Q-analysis and the structure of friendship networks. International Journal of Man-Machine Studies, 12(4), 367‑378. https://doi.org/10.1016/s0020-7373(80)80021-6

Latapy, M., Magnien, C., & Vecchio, N. D. (2008). Basic notions for the analysis of large two-mode networks. Social Networks, 30(1), 31‑48. https://doi.org/10.1016/j.socnet.2007.04.006

Maisonobe, M., Eckert, D., Grossetti, M., Jégou, L., & Milard, B. (2016). The world network of scientific collaborations between cities: domestic or international dynamics? Informetrics, 10, 1025‑1036. https://doi.org/https://doi.org/10.1016/j.joi.2016.06.002

Newman, M. E. J. (2001). Scientific collaboration networks. II. Shortest paths, weighted networks, and centrality. Physical Review E, 64(1). https://doi.org/10.1103/PhysRevE.64.016132

Opsahl, T. (2013). Triadic closure in two-mode networks: Redefining the global and local clustering coefficients. Social Networks, 35(2), 159‑167. https://doi.org/10.1016/j.socnet.2011.07.001

Padrón, B., Nogales, M., & Traveset, A. (2011). Alternative approaches of transforming bimodal into unimodal mutualistic networks. The usefulness of preserving weighted information. Basic and Applied Ecology, 12(8), 713‑721. https://doi.org/10.1016/j.baae.2011.09.004

Pizarro, N. (1999). Appartenances, places et réseaux de places. Sociologie et sociétés, 31(1), 143‑161. https://doi.org/10.7202/001568ar

Robins, G., & Alexander, M. (2004). Small Worlds Among Interlocking Directors: Network Structure and Distance in Bipartite Graphs. Computational & Mathematical Organization Theory, 10(1), 69‑94. https://doi.org/10.1023/B:CMOT.0000032580.12184.c0




